  import 'package:flutter/material.dart';

  class MyDrawer extends StatelessWidget {
    const MyDrawer({ Key? key,}) : super(key: key);
  
    @override
    Widget build(BuildContext context) {
      return  Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            // ignore: prefer_const_literals_to_create_immutables
            children: const [
              UserAccountsDrawerHeader(
                accountName: Text("Siyanda Phakathi"),
                accountEmail: Text("siyanda@gmail.com"),
                currentAccountPicture: CircleAvatar(backgroundColor: Colors.lightBlueAccent,
                child: Text(
                  "DP",
                  style: TextStyle(fontSize: 30.0),
                  ),
                ),
              ),
              ListTile(
                leading: Icon(Icons.home),
                title: Text("Home1"),
                trailing: Icon(Icons.arrow_forward_ios_rounded),
              ),
              ListTile(
                leading: Icon(Icons.person),
                title: Text("Profile"),
                trailing: Icon(Icons.arrow_forward_ios_rounded),
              ),
              ListTile(
                leading: Icon(Icons.settings),
                title: Text("Settings"),
                trailing: Icon(Icons.arrow_forward_ios_rounded),
              ),
              ListTile(
                leading: Icon(Icons.notifications),
                title: Text("Notifications"),
                trailing: Icon(Icons.arrow_forward_ios_rounded),
              ),
              ListTile(
                leading: Icon(Icons.contact_page),
                title: Text("Contact page"),
                trailing: Icon(Icons.arrow_forward_ios_rounded),
              ),
               ListTile(
                leading: Icon(Icons.help_center),
                title: Text("Help center"),
                trailing: Icon(Icons.arrow_forward_ios_rounded),
              ),
               ListTile(
                leading: Icon(Icons.favorite),
                title: Text("Fvourite"),
                trailing: Icon(Icons.arrow_forward_ios_rounded),
              ),
             
            ],
          ),
        );
    }
  }
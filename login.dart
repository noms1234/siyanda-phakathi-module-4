// ignore_for_file: prefer_const_constructors

import 'dart:html';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:splash_screen_take_2/homepage.dart';
import 'package:splash_screen_take_2/signup.dart';

class LoginPAge extends StatefulWidget {
  const LoginPAge({ Key? key }) : super(key: key);

  @override
  _LoginPAgeState createState() => _LoginPAgeState();
}

class _LoginPAgeState extends State<LoginPAge> {
  @override

loginRoute() {
  Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomePage(),
  ));
}

  Widget build(BuildContext context) {
    return  Scaffold(
      backgroundColor: Colors.white,
        body: SafeArea(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              // ignore: prefer_const_literals_to_create_immutables
              children: [
                SizedBox(height: 25),
                Text(
                  'Welcome',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 48,  
                  ),
                  ),
                  SizedBox(height: 80),
                  
                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                       border: Border.all(color: Colors.purple),
                       borderRadius: BorderRadius.circular(15),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextField(
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Enter your Email',
                            labelText: 'Email',
                          ),
                        ),
                      ),
                    ),
                  ),
                   SizedBox(height: 5),
                  
                   Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                       border: Border.all(color: Colors.purple),
                       borderRadius: BorderRadius.circular(15),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextField(
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Enter your Password',
                            labelText: 'Password',
                          ),
                          obscureText:true ,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 40,),
                   //login button
                  Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(30.0),
                        child: MaterialButton(
                          minWidth: double.infinity,
                          height: 60,
                          onPressed: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context) =>HomePage() ));
                          },
                          shape: RoundedRectangleBorder(
                            side: BorderSide(color: Colors.black
                            ),
                            borderRadius: BorderRadius.circular(50)
                          ),
                          child: Text(
                            "LogIn",
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 18,
                            ),
                            ),
                        ),
                      )
                    ],
                  ),
                   

                    SizedBox(height: 0.5),
                   //login button
                  Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(30.0),
                        child: MaterialButton(
                          minWidth: double.infinity,
                          height: 60,
                          onPressed: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context) => SignUp()));
                          },
                          color: Colors.purple,
                          shape: RoundedRectangleBorder(
                            side: BorderSide(color: Colors.purple,
                            ),
                            borderRadius: BorderRadius.circular(50)
                          ),
                          child: Text(
                            "SignUp",
                            
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 18,
                              color: Colors.white
                            ),
                            
                            ),
                            
                        ),
                      )
                    ],
                  ),
                   SizedBox(height: 10),

                   Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    // ignore: prefer_const_literals_to_create_immutables
                    children: [
                      Text('Don^t an accont?'),
                      Text(
                        'Register now',
                          style: TextStyle(
                          color: Colors.
                          blue,fontWeight: FontWeight.bold
                          ),
                        ),
                      ],
                    ),
                     ],
            ) 
            ),
          ),
    );
  }
}
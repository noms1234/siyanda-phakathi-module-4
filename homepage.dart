import 'package:flutter/material.dart';
import 'package:splash_screen_take_2/drawer.dart';

class HomePage extends StatelessWidget {
  const HomePage({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
          appBar: AppBar(
          centerTitle: true,
          elevation: 0,
          title: const Text(" Home "),
          actions: const [
           IconButton(
            icon: Icon(
              Icons.arrow_back_ios_new_rounded,
              color: Colors.white,
            ),
            onPressed: null,
           )
          ],
          ),
         floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
          drawer: MyDrawer(),
    );
  }
}
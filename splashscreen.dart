 import 'dart:async';
import 'dart:html';

import 'package:flutter/material.dart';
import 'package:splash_screen_take_2/homepage.dart';
import 'package:splash_screen_take_2/login.dart';

 class SplashScreen extends StatefulWidget {
   const SplashScreen({ Key? key }) : super(key: key);
 
   @override
   _SplashScreenState createState() => _SplashScreenState();
 }
 
 class _SplashScreenState extends State<SplashScreen> {
   @override
   void initState() {
    super.initState();
    Timer(const Duration(seconds: 5), () {
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (_) => LoginPAge(),
        ),
      );
    });
   }
   Widget build(BuildContext context) {
     return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Container(
          height: MediaQuery.of(context).size.height / 3,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/africa2.png"),
              ),
          ),
        )
          )
      
     );
   }
 }